package purse;

import java.util.Scanner;

public class Purse {
	
	static int balance=0;
	static void printBalance()
	{
		System.out.println("The remaining balance is"+balance);
	}
	
	static void addMoney(int money)
	{
		balance= balance+ money;
		printBalance();
	}
	
	static void  withdrawMoney(int money)
	{
		balance= balance- money;
		printBalance();
	}
	public static void main(String[] args)
	{
	int money;
	Scanner scan= new Scanner(System.in);
	
	printBalance();
	
	System.out.println("enter the amount to be added");
	money=scan.nextInt();
	addMoney(money);
	
	System.out.println("enter the amount to be withdrawn ");
	money=scan.nextInt();
	withdrawMoney(money);
	}
}
